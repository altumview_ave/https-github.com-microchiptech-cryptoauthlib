var a01160 =
[
    [ "hal_kit_hid_discover_buses", "a02163.html#gaec415116564275f0ed9ef020a973bb10", null ],
    [ "hal_kit_hid_discover_devices", "a02163.html#ga0121a73c96416c97ce07ed9077226588", null ],
    [ "hal_kit_hid_idle", "a02163.html#gaacf6310945889a6119cab867a1ced5e2", null ],
    [ "hal_kit_hid_init", "a02163.html#ga95b4e99655383680272a1d9354d42479", null ],
    [ "hal_kit_hid_post_init", "a02163.html#ga6f06bc1662b3d0361f6a241e1b5612f2", null ],
    [ "hal_kit_hid_receive", "a02163.html#ga4aa0c2792203529b7a07154229c355a3", null ],
    [ "hal_kit_hid_release", "a02163.html#ga8c7b9adb28b4bbdee8d5cd78ab16255f", null ],
    [ "hal_kit_hid_send", "a02163.html#ga3fd44b390fe7558c882c97faa783c018", null ],
    [ "hal_kit_hid_sleep", "a02163.html#ga1aa3345cb5d3e8fc29defe2758904fed", null ],
    [ "hal_kit_hid_wake", "a02163.html#ga65289262310f2662b2c47450b93c9580", null ],
    [ "kit_phy_num_found", "a02163.html#ga11ee6ec0b2b7eb2ff2472f92208c3205", null ],
    [ "kit_phy_receive", "a02163.html#gabb507252b1011037d6d2cce7d91b01d0", null ],
    [ "kit_phy_send", "a02163.html#gabd452e3edb32ea0d22653c182b4e1198", null ],
    [ "_gHid", "a02163.html#gab97bfae6ae6051d081edf51bb45eea05", null ]
];