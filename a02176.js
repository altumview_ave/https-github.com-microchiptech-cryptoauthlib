var a02176 =
[
    [ "ChipMode", "a02176.html#ad5dfaf88b2f7c2079cb1b503e4b9a788", null ],
    [ "Counter", "a02176.html#ac3349b57db0a391b282ecd30335e3c3e", null ],
    [ "I2C_Address", "a02176.html#aaf76c37c6666357af4812251568dbbda", null ],
    [ "I2C_Enable", "a02176.html#a0bfa7ef8796061592a7a45798d3f6f78", null ],
    [ "LastKeyUse", "a02176.html#a8fbbc38be4122319fb7eb0ac3ece5ee2", null ],
    [ "LockConfig", "a02176.html#a6a46a34796a3900bf63b43596d3591d3", null ],
    [ "LockValue", "a02176.html#a77182d11191823c8c1a88aa09f011967", null ],
    [ "OTPmode", "a02176.html#ae3202198cd9c5228a55992783934c8d2", null ],
    [ "Reserved0", "a02176.html#ada09753b1ff3dcdffa68f2efb935e9a1", null ],
    [ "Reserved1", "a02176.html#a604037992fe7e5fd08e1bcc684a1b12d", null ],
    [ "Reserved2", "a02176.html#aad9cf2ef13b53ee603c033aed0c3b986", null ],
    [ "RevNum", "a02176.html#afe35ea9abbbe191bbda642c1f3f608d8", null ],
    [ "Selector", "a02176.html#a2f0a7aaec62c815cd5eab47d82011a47", null ],
    [ "SlotConfig", "a02176.html#a3b3eefefab06c1773a81b7de899807c7", null ],
    [ "SN03", "a02176.html#a92ddb59a45be86354e40dae334dae659", null ],
    [ "SN47", "a02176.html#a0b88a67b771c165e6c7d601a87c253c7", null ],
    [ "SN8", "a02176.html#aeda063bff7e06b3f045d8f13366c1e3a", null ],
    [ "UserExtra", "a02176.html#a393190c1e9cafb845ca360b347fe0148", null ]
];