var a00911 =
[
    [ "ATCAIface", "a02159.html#ga75c11126f3fe81df3d6c27f5bb393303", null ],
    [ "ATCAIfaceType", "a02159.html#gac7f674d4a0afb12d2f3c9c5e98cb1089", [
      [ "ATCA_I2C_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a872efba3aa5b7890acfe0f1e0e453bcb", null ],
      [ "ATCA_SWI_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a2ce2e46b7f4245ab3d750ea2805fdc26", null ],
      [ "ATCA_UART_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a80ec8c070f1c711f2b16ad39d2a0486d", null ],
      [ "ATCA_SPI_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a8d50943acfc67f797b9c8fbf565deeb7", null ],
      [ "ATCA_HID_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a7da282c54282a02f5650c38a85d0dfb1", null ],
      [ "ATCA_CUSTOM_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089a1da1522f46e7dbd1a928ff130594561d", null ],
      [ "ATCA_UNKNOWN_IFACE", "a02159.html#ggac7f674d4a0afb12d2f3c9c5e98cb1089ac300244b5b323ee44001db318f365cec", null ]
    ] ],
    [ "ATCAKitType", "a02159.html#ga4a5c4554e7355754d28123f08f05a060", [
      [ "ATCA_KIT_AUTO_IFACE", "a02159.html#gga4a5c4554e7355754d28123f08f05a060a664bb9916e1b47bcf6ae414ffcec2b53", null ],
      [ "ATCA_KIT_I2C_IFACE", "a02159.html#gga4a5c4554e7355754d28123f08f05a060aaf3c1491bb0cb283b0ac35de770e2f45", null ],
      [ "ATCA_KIT_SWI_IFACE", "a02159.html#gga4a5c4554e7355754d28123f08f05a060a159c23e460fd4d21d0e9ec5fdeb648f3", null ],
      [ "ATCA_KIT_SPI_IFACE", "a02159.html#gga4a5c4554e7355754d28123f08f05a060a1ab1554696e001f082479a02f855210d", null ],
      [ "ATCA_KIT_UNKNOWN_IFACE", "a02159.html#gga4a5c4554e7355754d28123f08f05a060ac9a530c71bc570d6d58c2c8d521253a3", null ]
    ] ],
    [ "atgetifacecfg", "a02159.html#gac88ba81abfd42df65c6c0c64414dfc6e", null ],
    [ "atgetifacehaldat", "a02159.html#ga5e4163b8882d4eb42d4d5191c8731da0", null ],
    [ "atidle", "a02159.html#gac794fffe040e6d47a34c756720f3cbea", null ],
    [ "atinit", "a02159.html#ga386353e8700eec35e4548dfa29f13b8d", null ],
    [ "atpostinit", "a02159.html#ga6a9d6c47d866cba4ddd4ee6e671743d5", null ],
    [ "atreceive", "a02159.html#ga01badea388343bdf5929c5c2be9f426b", null ],
    [ "atsend", "a02159.html#gabd4f20b06efedede6bc4a836cfad8f38", null ],
    [ "atsleep", "a02159.html#gac06336335e5f3191e3b1fc06d2830d96", null ],
    [ "atwake", "a02159.html#ga32693c852341e1b946bab3cca5f71761", null ],
    [ "deleteATCAIface", "a02159.html#gaf8074d759241d3edd6d8ead1d7322a98", null ],
    [ "initATCAIface", "a02159.html#ga3a31087729a7a2e9a624572f234809fc", null ],
    [ "newATCAIface", "a02159.html#ga6f28f18f0d00c5301939724325f6b6fc", null ],
    [ "releaseATCAIface", "a02159.html#gab9ee16357a8e397a72eda7e9c8553fb3", null ]
];