var a02540 =
[
    [ "pNewPassword", "a02540.html#a57eb4fc15619633f034118e2ebf0c049", null ],
    [ "pNewPublicData", "a02540.html#a4213f41b59658caf27fa4f6a48a6acbe", null ],
    [ "pNewRandomA", "a02540.html#aac2f3b9bfbef00d0b000ee978c26deed", null ],
    [ "pOldPassword", "a02540.html#aa5768b5821839305e62ded58a4adb4c7", null ],
    [ "pOldPublicData", "a02540.html#a49f4cdf5ffabdadb1e1335242b05c150", null ],
    [ "pOldRandomA", "a02540.html#ac3978e19be0d7b37469e8a0533656469", null ],
    [ "pOldWrappedX", "a02540.html#ab66d64eff24b0fb44c976cbd06eb4813", null ],
    [ "ulNewPasswordLen", "a02540.html#a8e424df37706c0c5dc6b6f6e4bc47902", null ],
    [ "ulNewPublicDataLen", "a02540.html#a3b8260daaaf7c82eebde5d6a9619c87c", null ],
    [ "ulNewRandomLen", "a02540.html#a9c9eb19eea6e2d16c2ef58cf7b6f3261", null ],
    [ "ulOldPasswordLen", "a02540.html#a22aca150dbcc8624d4baf16654e818b0", null ],
    [ "ulOldPublicDataLen", "a02540.html#ab198fd92d80e111db8b47946178867fc", null ],
    [ "ulOldRandomLen", "a02540.html#ab59f51b5a5a26679244adcd0b3fe71e1", null ],
    [ "ulOldWrappedXLen", "a02540.html#aaebfa9ee1efa17e01385e912f131dbce", null ]
];