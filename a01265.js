var a01265 =
[
    [ "CK_CALLBACK_FUNCTION", "a01265.html#a5235e6437759c93b8189b124c8c807cf", null ],
    [ "CK_DECLARE_FUNCTION", "a01265.html#a30315d302108bcfb354196f37b16a492", null ],
    [ "CK_DECLARE_FUNCTION_POINTER", "a01265.html#aad472a68fb8e3eb9ba40169f5180b3b7", null ],
    [ "CK_PTR", "a01265.html#a423401496b51f5c72a74e5502b47fd7d", null ],
    [ "NULL_PTR", "a01265.html#a530f11a96e508d171d28564c8dc20942", null ],
    [ "PKCS11_API", "a01265.html#af7a984e00c3e5ad25e84e20064fdacf5", null ],
    [ "PKCS11_HELPER_DLL_EXPORT", "a01265.html#a836496360d58ba4ce6f85b9bd9db55fa", null ],
    [ "PKCS11_HELPER_DLL_IMPORT", "a01265.html#ac37148adb3a42adffda5a54520bafc5d", null ],
    [ "PKCS11_HELPER_DLL_LOCAL", "a01265.html#ad44738f0fc9eac7e23aed5fd4de71e4d", null ],
    [ "PKCS11_LOCAL", "a01265.html#aa160340f6de98552412dd32ce4e4f245", null ]
];