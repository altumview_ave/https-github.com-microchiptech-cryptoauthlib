var a02444 =
[
    [ "firmwareVersion", "a02444.html#a60a4109f4e2c04a2cd38f2b2b32c86f7", null ],
    [ "flags", "a02444.html#aab168fc251b8b32a3bb0639f4986fce1", null ],
    [ "hardwareVersion", "a02444.html#a13aace04822afda5af556b8eccfc447d", null ],
    [ "label", "a02444.html#afcbb066289e9ac53e4fa2f716928285b", null ],
    [ "manufacturerID", "a02444.html#a734f9aeeb46b5b72f35f7624900a7efe", null ],
    [ "model", "a02444.html#ad4eef90d5ee128bd3a5d85e7f583c633", null ],
    [ "serialNumber", "a02444.html#aa6bebb747d5f21cab37536f826d60c8b", null ],
    [ "ulFreePrivateMemory", "a02444.html#a039dbab2d84c79f65abf8b78d0f74ef4", null ],
    [ "ulFreePublicMemory", "a02444.html#a6c13a6a0eb580001cb0d0a21f977e75b", null ],
    [ "ulMaxPinLen", "a02444.html#a4c60397f414863f328338adf72b5981d", null ],
    [ "ulMaxRwSessionCount", "a02444.html#a9fd4e8ef694bceb1e6f770f8fd2e4433", null ],
    [ "ulMaxSessionCount", "a02444.html#a394870908a45cd7d77e9782bf8bc6084", null ],
    [ "ulMinPinLen", "a02444.html#a3b072cc87216ed127c027c402593844a", null ],
    [ "ulRwSessionCount", "a02444.html#a39de3a16823c0b86ad1c7eb3bdc96dee", null ],
    [ "ulSessionCount", "a02444.html#a2cb6413a03ac0656763d4f8a224c1dbf", null ],
    [ "ulTotalPrivateMemory", "a02444.html#af980569d979ee7f3e94424c58594607f", null ],
    [ "ulTotalPublicMemory", "a02444.html#a16e1947af9da868c8624d4628b6c8487", null ],
    [ "utcTime", "a02444.html#ae44f05ac955ad6b395ed5b11f9546b37", null ]
];