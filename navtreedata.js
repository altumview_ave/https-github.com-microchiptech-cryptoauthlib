/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CryptoAuthLib", "index.html", [
    [ "License", "a02709.html", null ],
    [ "calib directory - Purpose", "a02710.html", null ],
    [ "crypto directory - Purpose", "a02711.html", null ],
    [ "HAL Directory - Purpose", "a02712.html", null ],
    [ "mbedtls directory - Purpose", "a02713.html", null ],
    [ "openssl directory - Purpose", "a02714.html", null ],
    [ "IP Protection with Symmetric Authentication", "a02715.html", null ],
    [ "Setting up cryptoauthlib as a PKCS11 Provider for your system (LINUX)", "a02716.html", null ],
    [ "app directory - Purpose", "a02717.html", null ],
    [ "Secure boot using ATECC608", "a02718.html", null ],
    [ "TNG Functions", "a02719.html", null ],
    [ "CryptoAuthLib - Microchip CryptoAuthentication Library", "a02720.html", null ],
    [ "Deprecated List", "a02153.html", null ],
    [ "Todo List", "a02154.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", "globals_type" ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"a00011.html",
"a00914.html#a22bd6643f31f1d75dc3e7ea939f468cda2947392fc8a0455a9509335d3f64cb09",
"a00986.html#a3f7b636ed69136b5e5aa6d172cfd34e9",
"a00986.html#acbec80dc1b5f24d3013c2a9b06c11bdc",
"a01133.html#aa09ddc709367c497fc081a29aa2d12fe",
"a01379.html#a060692597a10ca395559a8a23b0b4fb8",
"a01379.html#a4c94c8a21456f846384a548253d33e95",
"a01379.html#a97dc21e338480e438bfcc28aca0c5d4d",
"a01379.html#adb69bd4cbb54f86eb2d518112c6d4d5d",
"a02155.html#ga416e4aeacf3acc35b1d5b76028c6947d",
"a02155.html#gada2d38835826587970de3868ca48eaa4",
"a02158.html#gab6e242074e10c6cf080d29325e9bca65",
"a02160.html#ga5430f01929cd7269e04db08d1f28642f",
"a02160.html#gga62a103735770a0f935a472fc2c1d78dbacfca1392e4cde6f2d467f9f69641890a",
"a02161.html#gac5046bb4ec9b6f7747b70ae9fb8a5922",
"a02163.html#ga4589d7b3e951f40b7928f1cf31f7ddf3",
"a02163.html#gaeaee19ff7623f0eab9839e68dae583af",
"a02166.html#ga72da31831a3647e287543634e3f78aca",
"a02167.html#ga89c04713bd2e0df331bd4f8f5abf14ab",
"a02168.html#ga82d3c529ed81119a956957052c06e8d7",
"a02288.html#a5eb439f0ede23956fde8cd72f41b85ba",
"a02448.html#aab168fc251b8b32a3bb0639f4986fce1",
"a02604.html#afcd59d3f53ba9541d524b464e52db6c3",
"globals_h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';