var a01223 =
[
    [ "KIT_MSG_SIZE", "a02163.html#ga0810d89c328e48fd91eb604323cca4e9", null ],
    [ "KIT_RX_WRAP_SIZE", "a02163.html#ga59e33124fc2fa800cc9c90a6ee3ff543", null ],
    [ "KIT_TX_WRAP_SIZE", "a02163.html#ga8a649555029d4f23edfc15a8c01b2d3d", null ],
    [ "kit_idle", "a02163.html#gaeaee19ff7623f0eab9839e68dae583af", null ],
    [ "kit_init", "a02163.html#ga8fb98d1c372e7e75f99a2f7d1182b877", null ],
    [ "kit_parse_rsp", "a02163.html#ga8bf59f4218d312568b769b8b8e5c563e", null ],
    [ "kit_receive", "a02163.html#gac31292c951353a25a81c7fe1308589c0", null ],
    [ "kit_send", "a02163.html#gacd6849349e1ba6ccbb4587a1d80562c8", null ],
    [ "kit_sleep", "a02163.html#gaf6054a0c971830411d5008b94923bdd8", null ],
    [ "kit_wake", "a02163.html#gaf7ce58d02b067b956abd32127f5519e9", null ],
    [ "kit_wrap_cmd", "a02163.html#ga0f09f4b79c681f54b88a8f71bca0d4c4", null ]
];