var a00035 =
[
    [ "pkcs11configLABEL_DEVICE_CERTIFICATE_FOR_TLS", "a00035.html#ad133b446f4dd937c7361ef008ebb5f28", null ],
    [ "pkcs11configLABEL_DEVICE_PRIVATE_KEY_FOR_TLS", "a00035.html#a38f9707de35016d7d7c52a6113f0d0df", null ],
    [ "pkcs11configLABEL_DEVICE_PUBLIC_KEY_FOR_TLS", "a00035.html#aabcf202a3a7ccad95b4388de2fccaee3", null ],
    [ "pkcs11configLABEL_JITP_CERTIFICATE", "a00035.html#a6687a6580a35d8b23bbbc9b678126885", null ],
    [ "pkcs11_config_cert", "a00035.html#a7afb784fad2dd8b8b2befd6159e2a053", null ],
    [ "pkcs11_config_key", "a00035.html#a0e5ec966dd01f9eb6f1cc2f4b3085f9f", null ],
    [ "pkcs11_config_load_objects", "a00035.html#a27c492a61f8600152b2ef86230d44c9b", null ],
    [ "atecc608_config", "a00035.html#ac78f00c0f99695504ba10c9c329a135b", null ]
];