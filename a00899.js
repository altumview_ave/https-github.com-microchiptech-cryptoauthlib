var a00899 =
[
    [ "ATCADeviceType", "a02158.html#gafb234ccd6a80d09c0efbe336c2354267", [
      [ "ATSHA204A", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a91729743caf308351a2b47c58536d268", null ],
      [ "ATECC108A", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a20efd97b5b1001eec4a52e0ed5bf594c", null ],
      [ "ATECC508A", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267af463439df0f95803fc57cc58bbff2dae", null ],
      [ "ATECC608A", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a183a6224a93e6c2b82c6dc0e132398bf", null ],
      [ "ATECC608B", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a6139512f9df589306dfc579a4e670d5a", null ],
      [ "ATECC608", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267af2f230354639df34317e81b1d9fb3dfe", null ],
      [ "ATSHA206A", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a6816489510886c97abe08d80c7207f3f", null ],
      [ "TA100", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a4af1fa9171e503676c56f45144d3b2f0", null ],
      [ "ATCA_DEV_UNKNOWN", "a02158.html#ggafb234ccd6a80d09c0efbe336c2354267a3488f672341dda0ad20508ad888280ad", null ]
    ] ]
];