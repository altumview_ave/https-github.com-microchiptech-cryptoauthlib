var a01199 =
[
    [ "change_i2c_speed", "a02163.html#gadc420e4b166a1b339d1a2687e0f12cb1", null ],
    [ "hal_i2c_discover_buses", "a02163.html#ga05700385437884cc7b28c38d33e84f8c", null ],
    [ "hal_i2c_discover_devices", "a02163.html#gac7644e62498a596c09952ee3b28017b9", null ],
    [ "hal_i2c_idle", "a02163.html#ga9317303ada721ff3f97e7aad69437a30", null ],
    [ "hal_i2c_init", "a02163.html#ga09e7f465a040fbd19ea136269571de3c", null ],
    [ "hal_i2c_post_init", "a02163.html#gafbe53519362f18c2688d10da6f7c618f", null ],
    [ "hal_i2c_receive", "a02163.html#ga41a2b07d55558b9726abe28fd8431e90", null ],
    [ "hal_i2c_release", "a02163.html#gac382cc4431d2d1c721c21a453ba36c22", null ],
    [ "hal_i2c_send", "a02163.html#gacc9a889e19d2f1a475a39391565b66dd", null ],
    [ "hal_i2c_sleep", "a02163.html#ga3f89bae6a625d5f37abdcc99408def27", null ],
    [ "hal_i2c_wake", "a02163.html#gad4df1b6755e2d5b5d9e810371e45ca8c", null ]
];