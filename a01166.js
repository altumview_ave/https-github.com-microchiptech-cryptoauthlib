var a01166 =
[
    [ "hal_spi_discover_buses", "a01166.html#a92cecc8e30a88dee1cbedd49141bc209", null ],
    [ "hal_spi_discover_devices", "a01166.html#a6cf45c115bcb721e951e8792e0feb3f0", null ],
    [ "hal_spi_idle", "a01166.html#a470458f44feb9f6dfb7845851d821182", null ],
    [ "hal_spi_init", "a01166.html#a6c7a1471361e74b4bf6ad7f0388715c2", null ],
    [ "hal_spi_open_file", "a01166.html#ac476728164468ba4e629bf2ad673a908", null ],
    [ "hal_spi_post_init", "a01166.html#aeb831d9894ce11146bd00c79a98b7f0f", null ],
    [ "hal_spi_receive", "a01166.html#afd849fdb15b7bd4a282a1207c53fb783", null ],
    [ "hal_spi_release", "a01166.html#a8c3f1c6c20d939c4ca976fdda6279359", null ],
    [ "hal_spi_send", "a01166.html#a15ac8afdddbb24792f0b627b53af99ef", null ],
    [ "hal_spi_sleep", "a01166.html#ae97ef6e553aed5ba6ac0e65476d82308", null ],
    [ "hal_spi_wake", "a01166.html#a182f605fabb3a02f9d4a0e433496290b", null ]
];