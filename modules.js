var modules =
[
    [ "Basic Crypto API methods (atcab_)", "a02155.html", "a02155" ],
    [ "Configuration (cfg_)", "a02156.html", null ],
    [ "ATCACommand (atca_)", "a02157.html", "a02157" ],
    [ "ATCADevice (atca_)", "a02158.html", "a02158" ],
    [ "ATCAIface (atca_)", "a02159.html", "a02159" ],
    [ "Certificate manipulation methods (atcacert_)", "a02160.html", "a02160" ],
    [ "Basic Crypto API methods for CryptoAuth Devices (calib_)", "a02161.html", "a02161" ],
    [ "Software crypto methods (atcac_)", "a02162.html", "a02162" ],
    [ "Hardware abstraction layer (hal_)", "a02163.html", "a02163" ],
    [ "Host side crypto methods (atcah_)", "a02164.html", "a02164" ],
    [ "JSON Web Token (JWT) methods (atca_jwt_)", "a02165.html", "a02165" ],
    [ "mbedTLS Wrapper methods (atca_mbedtls_)", "a02166.html", "a02166" ],
    [ "Attributes (pkcs11_attrib_)", "a02167.html", "a02167" ],
    [ "TNG API (tng_)", "a02168.html", "a02168" ]
];