var a02288 =
[
    [ "hal_data", "a02288.html#ad33508455720b78cc0fc880cb3f9885e", null ],
    [ "halidle", "a02288.html#a3939b643c7f807fc8fe8abcf18e99196", null ],
    [ "halinit", "a02288.html#aa020e68c9d18f83f205981fa57107b3c", null ],
    [ "halpostinit", "a02288.html#af174424ba7b2d19a74c72f8b4198c26b", null ],
    [ "halreceive", "a02288.html#aa7a6e850bf9584364224ce4d18898883", null ],
    [ "halrelease", "a02288.html#a5eb439f0ede23956fde8cd72f41b85ba", null ],
    [ "halsend", "a02288.html#a7256ddeb9e3ae126ac3b3d549bdb60e2", null ],
    [ "halsleep", "a02288.html#ad2f432748c4d8efe98ec42d5cd1552b5", null ],
    [ "halwake", "a02288.html#a033c21278fef7771916378cbcf726ae6", null ]
];