var a01151 =
[
    [ "hal_create_mutex", "a02163.html#gab289a3949663589ac6be71d72fb18278", null ],
    [ "hal_delay_10us", "a02163.html#gaa7df07f9efb18d8cef1d9602372aca8c", null ],
    [ "hal_delay_ms", "a02163.html#gadc23b8130e72a445f76c68d62e8c95c5", null ],
    [ "hal_delay_us", "a02163.html#ga7e9019810ba5ab81b256282392cd5079", null ],
    [ "hal_destroy_mutex", "a02163.html#ga4589d7b3e951f40b7928f1cf31f7ddf3", null ],
    [ "hal_lock_mutex", "a02163.html#gad4cd02ff7ae4e75844eab4e84eb61994", null ],
    [ "hal_unlock_mutex", "a02163.html#ga31fd8170a49623686543f6247b883bd1", null ]
];