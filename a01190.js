var a01190 =
[
    [ "hal_swi_discover_buses", "a02163.html#ga53864a301dac6efbdbdc3931aa79e23e", null ],
    [ "hal_swi_discover_devices", "a02163.html#ga354e6fa757688b73e6fb5293bb835def", null ],
    [ "hal_swi_idle", "a02163.html#ga618291172976da96c4f02dc9200c845c", null ],
    [ "hal_swi_init", "a02163.html#gaf406053c1a3f2a86bdc887f2ff4b663d", null ],
    [ "hal_swi_post_init", "a02163.html#ga9186e5235979216330ae580290872c77", null ],
    [ "hal_swi_receive", "a02163.html#ga159bdf9c24e429d54800da1eb1c21058", null ],
    [ "hal_swi_release", "a02163.html#ga196dbc62546b39ac3e556a0f7f6b7132", null ],
    [ "hal_swi_send", "a02163.html#ga9df4dbd666a87efe49578842e53ad91e", null ],
    [ "hal_swi_send_flag", "a02163.html#gaae96ea936a2e85d0bcb81b7868ee007d", null ],
    [ "hal_swi_sleep", "a02163.html#ga252e365a30f2583dae8a8eac073a9f53", null ],
    [ "hal_swi_wake", "a02163.html#gafc428980ec95e37eee94813db229fe70", null ]
];