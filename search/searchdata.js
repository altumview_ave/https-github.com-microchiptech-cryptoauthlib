var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvwxyz",
  1: "_achimst",
  2: "acehiklprst",
  3: "_abcdehiknoprst",
  4: "_abcdefghijklmnoprstuvwxyz",
  5: "acehips",
  6: "a",
  7: "acdst",
  8: "_abcdefghiklmnoprstuvw",
  9: "abchjmst",
  10: "acdhimost"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

