var searchData=
[
  ['kit_5fid_5ffrom_5fdevtype_4299',['kit_id_from_devtype',['../a02163.html#gacd2ec023c7cca9dff92c411d7c8bbb73',1,'kit_protocol.c']]],
  ['kit_5fidle_4300',['kit_idle',['../a02163.html#gaeaee19ff7623f0eab9839e68dae583af',1,'kit_protocol.c']]],
  ['kit_5finit_4301',['kit_init',['../a02163.html#ga8fb98d1c372e7e75f99a2f7d1182b877',1,'kit_protocol.c']]],
  ['kit_5finterface_5ffrom_5fkittype_4302',['kit_interface_from_kittype',['../a02163.html#ga6ba58bdf1ef6c9dabb64b56d51037c87',1,'kit_protocol.c']]],
  ['kit_5fparse_5frsp_4303',['kit_parse_rsp',['../a02163.html#ga8bf59f4218d312568b769b8b8e5c563e',1,'kit_protocol.c']]],
  ['kit_5fphy_5fnum_5ffound_4304',['kit_phy_num_found',['../a02163.html#ga11ee6ec0b2b7eb2ff2472f92208c3205',1,'hal_all_platforms_kit_hidapi.c']]],
  ['kit_5fphy_5freceive_4305',['kit_phy_receive',['../a02163.html#gabb507252b1011037d6d2cce7d91b01d0',1,'kit_phy_receive(ATCAIface iface, uint8_t *rxdata, int *rxsize):&#160;hal_all_platforms_kit_hidapi.c'],['../a02163.html#gaae5e94b6080d4279474eb3e98bc58901',1,'kit_phy_receive(ATCAIface iface, char *rxdata, int *rxsize):&#160;hal_win_kit_hid.c']]],
  ['kit_5fphy_5fsend_4306',['kit_phy_send',['../a02163.html#gabd452e3edb32ea0d22653c182b4e1198',1,'kit_phy_send(ATCAIface iface, uint8_t *txdata, int txlength):&#160;hal_all_platforms_kit_hidapi.c'],['../a02163.html#ga020fb0be6c73515122cf4b9f749fd97d',1,'kit_phy_send(ATCAIface iface, const char *txdata, int txlength):&#160;hal_win_kit_hid.c']]],
  ['kit_5freceive_4307',['kit_receive',['../a02163.html#gac31292c951353a25a81c7fe1308589c0',1,'kit_protocol.c']]],
  ['kit_5fsend_4308',['kit_send',['../a02163.html#gacd6849349e1ba6ccbb4587a1d80562c8',1,'kit_protocol.c']]],
  ['kit_5fsleep_4309',['kit_sleep',['../a02163.html#gaf6054a0c971830411d5008b94923bdd8',1,'kit_protocol.c']]],
  ['kit_5fwake_4310',['kit_wake',['../a02163.html#gaf7ce58d02b067b956abd32127f5519e9',1,'kit_protocol.c']]],
  ['kit_5fwrap_5fcmd_4311',['kit_wrap_cmd',['../a02163.html#ga0f09f4b79c681f54b88a8f71bca0d4c4',1,'kit_protocol.c']]]
];
