var annotated_dup =
[
    [ "_atecc508a_config", "a02180.html", "a02180" ],
    [ "_atecc608_config", "a02184.html", "a02184" ],
    [ "_atsha204a_config", "a02176.html", "a02176" ],
    [ "_pcks11_mech_table_e", "a02412.html", "a02412" ],
    [ "_pkcs11_attrib_model", "a02404.html", "a02404" ],
    [ "_pkcs11_lib_ctx", "a02408.html", "a02408" ],
    [ "_pkcs11_object", "a02416.html", "a02416" ],
    [ "_pkcs11_object_cache_t", "a02420.html", "a02420" ],
    [ "_pkcs11_session_ctx", "a02424.html", "a02424" ],
    [ "_pkcs11_slot_ctx", "a02428.html", "a02428" ],
    [ "atca_aes_cbc_ctx", "a02268.html", "a02268" ],
    [ "atca_aes_cmac_ctx", "a02272.html", "a02272" ],
    [ "atca_aes_ctr_ctx", "a02276.html", "a02276" ],
    [ "atca_aes_gcm_ctx", "a02252.html", "a02252" ],
    [ "atca_check_mac_in_out", "a02380.html", "a02380" ],
    [ "atca_command", "a02172.html", "a02172" ],
    [ "atca_decrypt_in_out", "a02376.html", "a02376" ],
    [ "atca_derive_key_in_out", "a02368.html", "a02368" ],
    [ "atca_derive_key_mac_in_out", "a02372.html", "a02372" ],
    [ "atca_device", "a02188.html", "a02188" ],
    [ "atca_gen_dig_in_out", "a02360.html", "a02360" ],
    [ "atca_gen_key_in_out", "a02388.html", "a02388" ],
    [ "atca_hmac_in_out", "a02356.html", "a02356" ],
    [ "atca_iface", "a02224.html", "a02224" ],
    [ "atca_include_data_in_out", "a02328.html", "a02328" ],
    [ "atca_io_decrypt_in_out", "a02336.html", "a02336" ],
    [ "atca_jwt_t", "a02396.html", "a02396" ],
    [ "atca_mac_in_out", "a02352.html", "a02352" ],
    [ "atca_nonce_in_out", "a02332.html", "a02332" ],
    [ "atca_secureboot_enc_in_out", "a02344.html", "a02344" ],
    [ "atca_secureboot_mac_in_out", "a02348.html", "a02348" ],
    [ "atca_sha256_ctx", "a02256.html", "a02256" ],
    [ "atca_sign_internal_in_out", "a02392.html", "a02392" ],
    [ "atca_temp_key", "a02324.html", "a02324" ],
    [ "atca_verify_in_out", "a02384.html", "a02384" ],
    [ "atca_verify_mac", "a02340.html", "a02340" ],
    [ "atca_write_mac_in_out", "a02364.html", "a02364" ],
    [ "atcacert_build_state_s", "a02248.html", "a02248" ],
    [ "atcacert_cert_element_s", "a02240.html", "a02240" ],
    [ "atcacert_cert_loc_s", "a02236.html", "a02236" ],
    [ "atcacert_def_s", "a02244.html", "a02244" ],
    [ "atcacert_device_loc_s", "a02232.html", "a02232" ],
    [ "atcacert_tm_utc_s", "a02228.html", "a02228" ],
    [ "ATCAHAL_t", "a02288.html", "a02288" ],
    [ "atcahid", "a02292.html", "a02292" ],
    [ "atcaI2Cmaster", "a02296.html", "a02296" ],
    [ "ATCAIfaceCfg", "a02192.html", "a02192" ],
    [ "ATCAPacket", "a02260.html", "a02260" ],
    [ "atcaSPImaster", "a02308.html", "a02308" ],
    [ "atcaSWImaster", "a02320.html", "a02320" ],
    [ "CK_AES_CBC_ENCRYPT_DATA_PARAMS", "a02532.html", "a02532" ],
    [ "CK_AES_CCM_PARAMS", "a02640.html", "a02640" ],
    [ "CK_AES_CTR_PARAMS", "a02624.html", "a02624" ],
    [ "CK_AES_GCM_PARAMS", "a02636.html", "a02636" ],
    [ "CK_ARIA_CBC_ENCRYPT_DATA_PARAMS", "a02652.html", "a02652" ],
    [ "CK_ATTRIBUTE", "a02452.html", "a02452" ],
    [ "CK_C_INITIALIZE_ARGS", "a02468.html", "a02468" ],
    [ "CK_CAMELLIA_CBC_ENCRYPT_DATA_PARAMS", "a02648.html", "a02648" ],
    [ "CK_CAMELLIA_CTR_PARAMS", "a02644.html", "a02644" ],
    [ "CK_CCM_PARAMS", "a02632.html", "a02632" ],
    [ "CK_CMS_SIG_PARAMS", "a02592.html", "a02592" ],
    [ "CK_DATE", "a02456.html", "a02456" ],
    [ "CK_DES_CBC_ENCRYPT_DATA_PARAMS", "a02528.html", "a02528" ],
    [ "CK_DSA_PARAMETER_GEN_PARAM", "a02656.html", "a02656" ],
    [ "CK_ECDH1_DERIVE_PARAMS", "a02480.html", "a02480" ],
    [ "CK_ECDH2_DERIVE_PARAMS", "a02484.html", "a02484" ],
    [ "CK_ECDH_AES_KEY_WRAP_PARAMS", "a02660.html", "a02660" ],
    [ "CK_ECMQV_DERIVE_PARAMS", "a02488.html", "a02488" ],
    [ "CK_FUNCTION_LIST", "a02400.html", "a02400" ],
    [ "CK_GCM_PARAMS", "a02628.html", "a02628" ],
    [ "CK_GOSTR3410_DERIVE_PARAMS", "a02684.html", "a02684" ],
    [ "CK_GOSTR3410_KEY_WRAP_PARAMS", "a02688.html", "a02688" ],
    [ "CK_INFO", "a02436.html", "a02436" ],
    [ "CK_KEA_DERIVE_PARAMS", "a02504.html", "a02504" ],
    [ "CK_KEY_DERIVATION_STRING_DATA", "a02596.html", "a02596" ],
    [ "CK_KEY_WRAP_SET_OAEP_PARAMS", "a02548.html", "a02548" ],
    [ "CK_KIP_PARAMS", "a02620.html", "a02620" ],
    [ "CK_MECHANISM", "a02460.html", "a02460" ],
    [ "CK_MECHANISM_INFO", "a02464.html", "a02464" ],
    [ "CK_OTP_PARAM", "a02608.html", "a02608" ],
    [ "CK_OTP_PARAMS", "a02612.html", "a02612" ],
    [ "CK_OTP_SIGNATURE_INFO", "a02616.html", "a02616" ],
    [ "CK_PBE_PARAMS", "a02544.html", "a02544" ],
    [ "CK_PKCS5_PBKD2_PARAMS", "a02600.html", "a02600" ],
    [ "CK_PKCS5_PBKD2_PARAMS2", "a02604.html", "a02604" ],
    [ "CK_RC2_CBC_PARAMS", "a02508.html", "a02508" ],
    [ "CK_RC2_MAC_GENERAL_PARAMS", "a02512.html", "a02512" ],
    [ "CK_RC5_CBC_PARAMS", "a02520.html", "a02520" ],
    [ "CK_RC5_MAC_GENERAL_PARAMS", "a02524.html", "a02524" ],
    [ "CK_RC5_PARAMS", "a02516.html", "a02516" ],
    [ "CK_RSA_AES_KEY_WRAP_PARAMS", "a02664.html", "a02664" ],
    [ "CK_RSA_PKCS_OAEP_PARAMS", "a02472.html", "a02472" ],
    [ "CK_RSA_PKCS_PSS_PARAMS", "a02476.html", "a02476" ],
    [ "CK_SEED_CBC_ENCRYPT_DATA_PARAMS", "a02692.html", "a02692" ],
    [ "CK_SESSION_INFO", "a02448.html", "a02448" ],
    [ "CK_SKIPJACK_PRIVATE_WRAP_PARAMS", "a02536.html", "a02536" ],
    [ "CK_SKIPJACK_RELAYX_PARAMS", "a02540.html", "a02540" ],
    [ "CK_SLOT_INFO", "a02440.html", "a02440" ],
    [ "CK_SSL3_KEY_MAT_OUT", "a02560.html", "a02560" ],
    [ "CK_SSL3_KEY_MAT_PARAMS", "a02564.html", "a02564" ],
    [ "CK_SSL3_MASTER_KEY_DERIVE_PARAMS", "a02556.html", "a02556" ],
    [ "CK_SSL3_RANDOM_DATA", "a02552.html", "a02552" ],
    [ "CK_TLS12_KEY_MAT_PARAMS", "a02672.html", "a02672" ],
    [ "CK_TLS12_MASTER_KEY_DERIVE_PARAMS", "a02668.html", "a02668" ],
    [ "CK_TLS_KDF_PARAMS", "a02676.html", "a02676" ],
    [ "CK_TLS_MAC_PARAMS", "a02680.html", "a02680" ],
    [ "CK_TLS_PRF_PARAMS", "a02568.html", "a02568" ],
    [ "CK_TOKEN_INFO", "a02444.html", "a02444" ],
    [ "CK_VERSION", "a02432.html", "a02432" ],
    [ "CK_WTLS_KEY_MAT_OUT", "a02584.html", "a02584" ],
    [ "CK_WTLS_KEY_MAT_PARAMS", "a02588.html", "a02588" ],
    [ "CK_WTLS_MASTER_KEY_DERIVE_PARAMS", "a02576.html", "a02576" ],
    [ "CK_WTLS_PRF_PARAMS", "a02580.html", "a02580" ],
    [ "CK_WTLS_RANDOM_DATA", "a02572.html", "a02572" ],
    [ "CK_X9_42_DH1_DERIVE_PARAMS", "a02492.html", "a02492" ],
    [ "CK_X9_42_DH2_DERIVE_PARAMS", "a02496.html", "a02496" ],
    [ "CK_X9_42_MQV_DERIVE_PARAMS", "a02500.html", "a02500" ],
    [ "CL_HashContext", "a02280.html", "a02280" ],
    [ "hid_device", "a02304.html", "a02304" ],
    [ "hw_sha256_ctx", "a02264.html", "a02264" ],
    [ "i2c_sam0_instance", "a02312.html", "a02312" ],
    [ "i2c_sam_instance", "a02316.html", "a02316" ],
    [ "i2c_start_instance", "a02300.html", "a02300" ],
    [ "memory_parameters", "a02704.html", "a02704" ],
    [ "secure_boot_config_bits", "a02696.html", "a02696" ],
    [ "secure_boot_parameters", "a02700.html", "a02700" ],
    [ "sw_sha256_ctx", "a02284.html", "a02284" ],
    [ "tng_cert_map_element", "a02708.html", "a02708" ]
];