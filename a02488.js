var a02488 =
[
    [ "hPrivateData", "a02488.html#a2f6769e4ceda4853db07b96b8e5770a0", null ],
    [ "kdf", "a02488.html#a0b7cdb5c49b5009e5d66ca1d6b6ead43", null ],
    [ "pPublicData", "a02488.html#a5b5ab88b8cf93cfa701f56237f237292", null ],
    [ "pPublicData2", "a02488.html#a807f984f215aef03dc5ac126804618dd", null ],
    [ "pSharedData", "a02488.html#acab261b5f4c0104ea7011d671b371ab1", null ],
    [ "publicKey", "a02488.html#afefc9f01b422602a32acbd4fd4bb9ccc", null ],
    [ "ulPrivateDataLen", "a02488.html#a298011aaedd3c092f893e8bbba354cf9", null ],
    [ "ulPublicDataLen", "a02488.html#ae9ccaf64b51247d789c8e4d78b64b54c", null ],
    [ "ulPublicDataLen2", "a02488.html#a883bd3806b7c0e47291bc8efce44ccc1", null ],
    [ "ulSharedDataLen", "a02488.html#a95606c7e5bb43c1d62f41791c98d7f7f", null ]
];